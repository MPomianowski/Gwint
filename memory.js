var cards = ["ciri.png", "geralt.png", "jaskier.png", "jaskier.png", "geralt.png","ciri.png", "triss.png", "iorweth.png", "iorweth.png", "triss.png", "yen.png", "yen.png" ];

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
}

shuffle(cards);

//var markers = [];
//
// for(var i=0; i<cards.length; i++) {
//     markers[i] = "var c" + i;
// }
//
// for(var i=0; i<cards.length; i++) {
//     markers[i] = $("#c"+i);
// }
// $(c1).click(function (){console.log(1)})
//
// for(var i=0; i<cards.length; i++) {
//     markers[i] = $('c'+i).click(function (){console.log(i)});
// }

$(c0).click(function (){revealCard(0);});
$(c1).click(function (){revealCard(1);});
$(c2).click(function (){revealCard(2);});
$(c3).click(function (){revealCard(3);});
$(c4).click(function (){revealCard(4);});
$(c5).click(function (){revealCard(5);});
$(c6).click(function (){revealCard(6);});
$(c7).click(function (){revealCard(7);});
$(c8).click(function (){revealCard(8);});
$(c9).click(function (){revealCard(9);});
$(c10).click(function (){revealCard(10);});
$(c11).click(function (){revealCard(11);});
$('#again').click(function (){location.reload();});


var oneVisible = false;
var turnCounter = 0;
var visible_nr;
var lock = false;
var pairsLeft = 6;

function revealCard(nr)
{
    var opacityValue = $('#c'+nr).css('opacity');

    if (opacityValue != 0 && lock == false) {
        lock = true;
        var obraz = "url(img/" + cards[nr] + ")";

        $('#c'+nr).css('background-image', obraz);
        $('#c'+nr).addClass('cardA');
        $('#c'+nr).removeClass('card');

        if(oneVisible == false){
            // first card
            oneVisible = true;
            visible_nr = nr;
            lock = false;
        } else {
            // second card
            if (cards[visible_nr] == cards[nr] && visible_nr != nr) {
                // para
                setTimeout(function () {
                    hide2Cards(nr, visible_nr)
                }, 750);
            } else {
                // pudło
                setTimeout(function () {
                    restore2Cards(nr, visible_nr)
                }, 1000);
            }
            turnCounter++;
            $('.score').html('Liczba rund: '+turnCounter);
            oneVisible = false;
        }
    }

}

function hide2Cards(nr1, nr2){
    $('#c'+nr1).css('opacity', '0');
    $('#c'+nr2).css('opacity', '0');

    pairsLeft--;

    if(pairsLeft == 0){
        $('.board').html('<h1>Wygrałeś!<br>Licza rund: '+turnCounter+'</h1>');
    }

    lock = false;
}

function restore2Cards(nr1, nr2){
    $('#c'+nr1).css('background-image', 'url(img/karta.png)');
    $('#c'+nr1).addClass('card');
    $('#c'+nr1).removeClass('cardA');
    $('#c'+nr2).css('background-image', 'url(img/karta.png)');
    $('#c'+nr2).addClass('card');
    $('#c'+nr2).removeClass('cardA');

    lock = false;
}
